<?php

/**
 * @file
 * Namespice module. Emulates cache namespace using concept of cache tags.
 */

/**
 * Wrapper around cache_get(). Checks tag serials first, with first missing
 * serial resulting in cache miss.
 * 
 * Need to provide same combination of tags we did with ns_cache_set() in
 * order to get same entry back.
 * 
 * @param $cid
 *  Cache id.
 * @param $table
 *  Cache table. Default is 'cache'.
 * @param $tags
 *  Array of cache tags. Each tag is appended to cache id together with it's 
 *  serial. Order of tags in array doesn't matter.
 * @param $serial_table
 *  Table to store serials. If not provided, $table is used.
 *      
 * @return unknown_type
 *  Same as cache_get(): cache object or FALSE in case of cache miss.
 *  
 * TODO: It's possible to insert cache performance logging here. It's very 
 * easy to do it here and not rely on memcache statistics so statistics 
 * becomes available for every cache backend. This feature was once implemented
 * at Cache Router but was removed. If it's implemented here, it will work for
 * any cache backend. Hardest part would be UI, charts, and all that stuff, huh. 
 */
function ns_cache_get($cid, $table = 'cache', $tags = array(), $serial_table = NULL) {
  sort($tags); // We want exactly same order of tags.
  if (!$serial_table) {
    $serial_table = $table;
  }
  foreach ($tags as $tag) {
    $serial = ns_serial('check', $tag, $serial_table);
    if (!$serial) {
      // Log miss.
      return FALSE;
    }
    $cid .= ':' . $tag .':'. $serial;
  }
  $cache = cache_get($cid, $table);
  if (!$cache) {
    // Log miss.
    return FALSE;
  }
  else {
    // Log hit.
    return $cache;
  }
}

/**
 * Wrapper around cache_set(). Appends tags and their serials to $cid.
 * 
 * @param $cid
 *  Cache id.
 * @param $data
 *  Cache data.
 * @param $table
 *  Cache table. Default is 'cache'.
 * @param $tags
 *  Array of cache tags. Each tag is appended to cache id together with it's 
 *  serial. Order of tags in array doesn't matter.
 * @param $serial_table
 *  Table to store serials. If not provided, $table is used.
 */
function ns_cache_set($cid, $data, $table = 'cache', $tags = array(), $serial_table = NULL) {
  sort($tags); // We want exactly same order of tags
  if (!$serial_table) {
    $serial_table = $table;
  }
  foreach ($tags as $tag) {
    $cid .= ':' . $tag .':'. ns_serial('get', $tag, $serial_table);
  }
  cache_set($cid, $data, $table);
}


/**
 * Invalidate tagged cache entries.
 * 
 * Uses logics adapted to namespaces (tags): instead of flushing content cache,
 * we simply increase serial of each provided tag.
 * To flush specific entry, in addition to $cid we need to provide same 
 * combination of tags as we did with ns_cache_set().
 * 
 * @param $cid string
 *  Cache id of single entry operations, or '*' for mass operations. Single
 *  operation clears exactly one entry with key equal to $cid with appended
 *  tags+serials.
 * @param $table string
 *  Cache table to work with. Default is 'cache'.
 * @param $tags array
 *  Array of cache tags. If $cid is '*', function clears cache entries tagged 
 *  with ANY (not all!) tag in the combination (by increasing serial for 
 *  every tag).
 * @param $serial_table
 *  Table to store serials. If not provided, $table is used.
 *  
 * TODO: To support DB cache backend, we need to implement flushing of 
 * content cache itself otherwise DB may fill up and crash server.
 * It would require backend detection so we don't do this for memcache.
 * 
 */
function ns_cache_clear_all($cid = '*', $table = 'cache', $tags = array(), $serial_table = NULL) {
  if (!$serial_table) {
    $serial_table = $table;
  }
  if ($cid == '*') {
    // Flush tagged cache entries by increasing their serials.
    foreach ($tags as $tag) {
      ns_serial('increment', $tag, $serial_table);
    }
  }
  else {
    // Single entry flush.
    foreach ($tags as $tag) {
      $cid .= ':' . $tag . ':' . ns_serial('get', $tag, $serial_table);
    }
    cache_clear_all($cid, $table);
  }
}

/**
 * Perform simple tag serial operations. 
 * 
 * Uses static cache so during one page request we get serial from cache 
 * only once per tag (unless we explicitly flush static cache).
 * 
 * @param $op string
 *  One of the following operations:
 *  "get"       - initializes if needed and returns current serial.
 *  "increment" - increments, initializes if needed, and returns serial.
 *  "check"     - checks backend-stored serial of tag. Returns serial on
 *                success; in case of miss initializes serial and returns FALSE.
 *  "flush"     - flushes specific tag static serial cache, or entire
 *                static serial cache, if $tag is NULL.
 *                
 * @param $tag string
 *   Target tag or NULL for special wildcard operations such as "flush".
 * 
 * @param $table string
 *   Cache table where serials for namespace are stored, not needed for special
 *   operations such as 'flush'. Default is 'cache'.
 * 
 * @return
 *   For most operations returns node serial number.
 */
function ns_serial($op, $tag = NULL, $table = 'cache') {
  static $serials;
  if (!is_array($serials)) {
    $serials = array();
  }
  $cid = 'serial_' . $tag;
  
  switch ($op) {
    
    case 'get':
      // Check static cache.
      if (isset($serials[$tag])) {
        return $serials[$tag];
      }
      $cache = cache_get($cid, $table);
      if (!$cache) {
        // Serial cache miss. Initialize serial for that node.
        $serials[$tag] = rand(1, 10000);
        cache_set($cid, $serials[$tag], $table);
      }
      else {
        $serials[$tag] = $cache->data;
      }
      return $serials[$tag];
      
    case 'increment':
      // Check static cache.
      if (isset($serials[$tag])) {
        $serials[$tag]++;
      }
      else {
        // Check memcache.
        $cache = cache_get($cid, $table);
        if (!$cache) {
          $serials[$tag] = rand(1, 10000); // Initialize serial.
        }
        else {
          $serials[$tag] = $cache->data + 1;
        }
      }
      // Write new serial to memcache.
      cache_set($cid, $serials[$tag], $table); 
      return $serials[$tag];
    
    case 'check':
      // Check static cache first. If serial is missing, it may be in 
      // memcache (that means this is first request of the serial in the 
      // current page request), so we check there too.
      // False positives are harmless: it will result in just 1 useless backend 
      // request.
      if (isset($serials[$tag])) {
        return $serials[$tag];
      }
      $cache = cache_get($cid, $table);
      if (!$cache) {
        $serials[$tag] = rand(1, 10000); // Initialize serial.
        cache_set($cid, $serials[$tag], $table);
        return FALSE;
      }
      else {
        $serials[$tag] = $cache->data;
        return $serials[$tag];
      }
    
    case 'flush':
      if (!empty($tag)) {
        unset($serials[$tag]);
      }
      else {
        $serials = array();
      }
  }
}
